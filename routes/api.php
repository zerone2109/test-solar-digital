<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/comments', 'CommentsController@index')->name('show.comments');
Route::post('/add-comment', 'CommentsController@add')->name('add.comment');
Route::put('/update-comment','CommentsController@update')->name('update.comment');
Route::delete('/delete-comment','CommentsController@delete')->name('delete.comment');
