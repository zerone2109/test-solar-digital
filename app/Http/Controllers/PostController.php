<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use View;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::first();
        return view('welcome', compact('post'));

    }
}
