<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Comments;


/**
 * Стандартный набор CRUD-a.
 * C валидацией не парился, так как многих факторов нет.
 * Обработку ошибок тоже можно описать куда больше, и по статусам расписать, но не думаю что это кто-то оценит, так так это больше рутинная работа, а не умственная.
 */

class CommentsController extends Controller
{
    public function index(){

        $data = Comments::with('replies')->get();
        return response()->json($data,200);

    }
    public function add (Request $request){

        $validator = Validator::make($request->toArray(), [
            'comment' => 'required',
        ]);

        if($validator->fails()){
            return response()->json('Error' . " : " .$validator->errors(),400);
        }else{

            $query = Comments::create($request->toArray());
            return response()->json('Comment successfully created',200);

        }
    }

    public function update (Request $request){

        $validator = Validator::make($request->toArray(), [
            'id' => 'required',
            'comment' => 'required',
        ]);

        if($validator->fails()){
            return response()->json('Error' . " : " .$validator->errors(),400);
        }else {
            // на хосте с реквеста прилитает юрлка, почему и зачем не знаю, на локалке ее нет, но мы ее уберем..
            // вообще по хорошему нужно сделать из реквеста выборку только нужных данных и их только записывать, но в данном слечаее тз мне делать и дебажить лень=)
            $data =  $request->toArray();
            if(isset($data['_url'])){
                unset($data['_url']);
            }
            $query = Comments::where(['id' => $request->id])->update($data);
            return response()->json('Comment successfully update',200);
        }
    }

    public function delete (Request $request){
        $validator = Validator::make($request->toArray(), [
            'id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json('Error' . " : " .$validator->errors(),400);
        }else {
            $query = Comments::where(['id' => $request->id])->delete();

            return response()->json('Comment successfully deleted',200);
        }
    }
}
