<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'text',
        'parent_id'
    ];

    public function comments()
    {
        return $this->hasMany(Comments::class)->whereNull('parent_id');
    }
}
