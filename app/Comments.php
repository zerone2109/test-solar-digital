<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'id',
        'post_id',
        'comment',
        'parent_id'
    ];

    public function replies()
    {
        return $this->hasMany(Comments::class, 'parent_id');
    }
}
