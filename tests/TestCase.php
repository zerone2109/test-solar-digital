<?php
namespace Tests;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;
    protected $faker;
    public function setUp():void {
        parent::setUp();
        Artisan::call('migrate --path=/app/database/migrations/2019_07_30_171856_comments_table.php');
        Artisan::call(
            'db:seed', ['--class' => 'DatabaseSeeder']
        );
        $this->faker = Factory::create();
    }
   
}
