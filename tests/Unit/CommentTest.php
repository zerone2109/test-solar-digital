<?php
namespace Tests\Unit;
use App\Comments;
use Tests\TestCase;

/**
 * И так до можно до утра их писать. А лучше скачать Postman и как бы все быстрее сделать в данном случае с api имхо...
 * Да, знаю для тестов нужно бы на другую бд переехать, что бы "боевую" не трогать, но она у меня 2 в 1 =)
 */
class CommentTestTest extends TestCase
{
    public function test_can_show_all_comment() {
            $this->get(route('show.comments'))
            ->assertStatus(200);
    }

    public function test_can_create_comment() {
        $data = [
            'comment' => $this->faker->sentence,
            'post_id' => 1,
        ];
        $this->post(route('add.comment'), $data)
            ->assertStatus(200);

    }
    public function test_can_update_comment() {
        $data = [
            'id' => 1,
            'comment' => $this->faker->sentence,
            'post_id' => 1,
        ];
        $this->put(route('update.comment'), $data)
            ->assertStatus(200);

    }
    public function test_can_delete_comment() {
        $data = [
            'id' => 3,
        ];
        $this->delete(route('delete.comment'), $data)
            ->assertStatus(200);

    }

    public function test_can_create_comment_with_empty_data() {
        $data = [];
        $this->delete(route('delete.comment'), $data)
            ->assertStatus(400);

    }

    public function test_can_update_comment_with_empty_comment() {
        $data = [
            'id' => 1,
            'post_id' => 1,
        ];
        $this->put(route('update.comment'), $data)
            ->assertStatus(400);

    }

    public function test_can_delete_comment_with_empty_id() {
        $data = [ ];
        $this->delete(route('delete.comment'), $data)
            ->assertStatus(400);

    }


}
