<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Comments::class, function (Faker $faker) {
    return ['comment' => $faker->sentence,'post_id' => '1','comment_id' => 1];
});
