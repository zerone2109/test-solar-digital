<?php

use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            [
                'post_id'=> 1,
                'comment'=>'Коммент №1',
                'parent_id' => null
            ],
            [
                'post_id'=> 1,
                'comment'=>'Коммент №2',
                'parent_id' => null
            ],
            [
                'post_id'=> 1,
                'comment'=>'Ответ к 1 комменту',
                'parent_id' => 1
            ],
            [
                'post_id'=> 1,
                'comment'=>'Ответ к 1 ответу',
                'parent_id' => 3
            ],
            [
                'post_id'=> 1,
                'comment'=>'Ответ к 2 комменту',
                'parent_id' => 2
            ],
            [
            'post_id'=> 1,
            'comment'=>'Ответ к 1 комменту #2 ',
            'parent_id' => 1
        ]
        ]);
    }
}
