<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Comment API</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Styles -->

</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-4" style="text-align: center">{{$post->title}}</h1>
        <hr class="my-4">
        <p class="lead"><pre>{!! $post->text !!}</pre></p>
        <div style="text-align: center"><a href="https://bitbucket.org/dashboard/overview" target="_blank">КОД ТУТ</a></div>
        <hr class="my-4">
        <div class="comments">
            <h3>Коменты</h3>
            @include('comments', ['comments' => $post->comments, 'post_id' => $post->id])
        </div>
    </div>

</div>


</body>
</html>
