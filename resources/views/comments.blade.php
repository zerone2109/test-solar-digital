@foreach($comments as $comment)
    <ul class="list-group">
        <li class="list-group-item disabled" aria-disabled="true" @if($comment->parent_id != null) style="margin-left:40px;border-bottom-width: 0px; border-right-width: 0px; border-top-width: 0px;" @endif>
            <p>{{ $comment->comment }} [ id : {{$comment->id}}  @if(isset($comment->parent_id)) , parent_id : {{$comment->parent_id}} @endif ]
            </p>
            @include('comments', ['comments' => $comment->replies])
        </li>
    </ul>

@endforeach
